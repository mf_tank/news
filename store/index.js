import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = () => new Vuex.Store({
    state: {
        newses: {},
        news: {}
    },
    mutations: {
        setNewses(state, array) {
            state.newses = array;
        },
        setNews(state, object) {
            state.news = object;
        },
    },
    actions: {
      async fetchNewses({ commit }) {
        let response;
        try {
          response = await this.$axios.$get('/news', {
            validateStatus: function (status) {
              return status !== 500 && status !== 404;
            }
          });

          if (response.status) {
            commit("setNewses", response.data);
          }

        } catch(e) {
          console.error('ошибка fetchNewses' + e);
        }
      },
      async login({commit}) {
        this.$auth.loginWith('laravelJWT', {
          data: {
            email: 'skugarev.andrey@gmail.com',
            password: '123456789'
          }
        })
      }
    },
    getters: {
        getNewses: state => {
          return state.newses;
        },
        getNews: state => {
            return state.news
        },
    }

});

export default store
